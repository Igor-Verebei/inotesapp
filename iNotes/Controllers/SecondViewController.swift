
import UIKit
import RealmSwift

class SecondViewController: UIViewController {
    
    var oneNote = Note()
    @IBOutlet weak var headTextField: UITextField!
    @IBOutlet weak var bodyTextView: UITextView!
    @IBOutlet weak var doneButtonOutlet: UIButton!
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let recognizer = UISwipeGestureRecognizer(target: self, action: #selector(backButton))
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)
        
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillShow),name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(keyboarddidHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.headTextField.text = oneNote.head
        self.bodyTextView.text = oneNote.body
        self.doneButtonOutlet.isHidden = true
        if oneNote.head == "" && oneNote.body == "" {
            self.headTextField.becomeFirstResponder()
        }
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        self.doneButtonOutlet.isHidden = false
    }
    
    @objc func keyboarddidHide(_ notification: Notification) {
        self.doneButtonOutlet.isHidden = true
    }
    
    @IBAction func doneButton(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        
        navigationController?.popViewController(animated: true)
        
        let newNote = Note()
        newNote.head = self.headTextField.text ?? ""
        newNote.body = self.bodyTextView.text ?? ""
        
        if self.bodyTextView.text != "" || self.headTextField.text != "" {
            do {
                try  self.realm.write({
                    self.realm.add(newNote)
                })
            } catch let error {
                print(error)
            }
            
            do {
                try  self.realm.write({
                    self.realm.delete(self.oneNote)
                })
            } catch let error {
                print(error)
            }
        }
            
        else {
            
            do {
                try  self.realm.write({
                    self.realm.delete(self.oneNote)
                })
            } catch let error {
                print(error)
            }
        }
    }
}
