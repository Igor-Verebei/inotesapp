
import UIKit
import RealmSwift

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var notesArray =  Manager.shared.getNotes()
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(Realm.Configuration.defaultConfiguration.fileURL!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    @IBAction func save(_ sender: UIButton) {
        
        let note = Note()
        
        do {
            try  realm.write({
                realm.add(note)
            })
        } catch let error {
            print(error)
        }
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController else {return}
        controller.oneNote = note
        self.navigationController?.pushViewController(controller, animated: true)
        
        tableView.reloadData()
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notesArray.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)  as? CustomTableViewCell else {return UITableViewCell()}
        cell.set(allNotes: notesArray[indexPath.row])
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.brown.withAlphaComponent(0.3)
        cell.selectedBackgroundView = backgroundView
        
        return cell
        
        
        
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController else {return}
        controller.oneNote = notesArray[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        let item = realm.objects(Note.self)[indexPath.row]
        try! realm.write {
            realm.delete(item)
        }
        tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade)
        tableView.reloadData()
    }
}

