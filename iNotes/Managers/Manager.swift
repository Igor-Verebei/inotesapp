
import Foundation
import RealmSwift

class Manager {
    
    static var shared = Manager()
    private init(){}
    
    func getNotes() -> Results<Note>  {
         
         let realm = try! Realm()
         let notes = realm.objects(Note.self)
         print(notes)
         return notes
     }
}
