
import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var headLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!

    
    func set(allNotes: Note){
        self.headLabel.text = allNotes.head
        self.bodyLabel.text = allNotes.body
    }
}
